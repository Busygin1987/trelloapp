export const FETCH_BOARDS = 'FETCH_BOARDS';
export const DELETE_BOARDS = 'DELETE_BOARDS';
export const FETCH_BOARDS_SUCCESS = 'FETCH_BOARDS_SUCCESS';
export const FETCH_BOARDS_FAILURE = 'FETCH_BOARDS_FAILURE';
export const SET_ID_BOARD = 'SET_ID_BOARD';

interface FetchBoards {
  type: typeof FETCH_BOARDS
  payload?: []
}

interface FetchBoardsSuccess {
  type: typeof FETCH_BOARDS_SUCCESS
  payload: Array<Object>
}

interface FetchBoardsFailure {
  type: typeof FETCH_BOARDS_FAILURE
  payload?: []
}

interface DeleteBoards {
  type: typeof DELETE_BOARDS
  payload?: []
}

interface SetIdBoard {
  type: typeof SET_ID_BOARD
  payload: IObject
}

export interface IObject {
  id: string
  name: string
}

export type FetchBoardActionTypes = FetchBoardsSuccess | FetchBoards | FetchBoardsFailure | DeleteBoards | SetIdBoard