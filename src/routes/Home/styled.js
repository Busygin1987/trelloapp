import styled from 'styled-components';

export const WrapContainer = styled.div`
  width: auto;
  height: calc(100vh - 80px);
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const UnderlineText = styled.span`
  text-decoration: underline;
  cursor: pointer;
`;