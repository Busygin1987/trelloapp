import { SET_ID_BOARD, FETCH_BOARDS, FETCH_BOARDS_SUCCESS, FETCH_BOARDS_FAILURE, DELETE_BOARDS, FetchBoardActionTypes, IObject } from './types';

interface IState {
    boards: Array<Object>
    error: boolean
    loading: boolean
    currentBoard: IObject
}

let initialState: IState = {
    boards: [],
    error: false,
    loading: false,
    currentBoard: {
      id: '',
      name: ''
    }
}

export function homeReducer(
  state = initialState,
  action: FetchBoardActionTypes
): IState {
  switch (action.type) {
    case FETCH_BOARDS: {
        return {
          ...state,
          loading: true
        }
    }
    case FETCH_BOARDS_SUCCESS: {
        return {
          ...state,
          loading: false,
          boards: action.payload
        }
    }
    case FETCH_BOARDS_FAILURE: {
        return {
          ...state,
          loading: false,
          error: true
        }
    }
    case DELETE_BOARDS: {
        return {
          ...state,
          boards: []
        }
    }
    case SET_ID_BOARD: {
      return {
        ...state,
        currentBoard: action.payload
      }
    }
    default:
      return state
  }
}
