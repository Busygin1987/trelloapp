import React from 'react';
// import styled from 'styled-components';
import Typography from '../../components/Typography';
import { connect } from 'react-redux';
import { loginAction } from '../../modules/Header/actions';
import { WrapContainer, UnderlineText } from './styled';
import SelectBoard from '../../modules/SelectBoard';
import { setIdBoard } from './actions';

interface StateProps {
  boards: Array<object>
}
     
interface DispatchProps {
  login: () => void
  setBoardId: (id: string, name: string) => void
}

interface OwnProps {
  
}

type Props = StateProps & DispatchProps & OwnProps

const prepareBoards = (boards: Array<object>) => {
  return boards.map((board: any): any => {
    return {
      name: board.name,
      id: board.id
    };
  });
}

const Home: React.FC<Props> = ({ login, boards, setBoardId }) => {
  return (
    <WrapContainer>
      {
        boards && boards.length > 0
          ? 
            <SelectBoard boards={prepareBoards(boards)} setBoardId={setBoardId} />
          : <Typography>
              Please, <UnderlineText onClick={login}>login via Trello</UnderlineText> first to proceed...
            </Typography>
      }
    </WrapContainer>
  );
}

const mapStateToProps = (state: any) => ({
  boards: state.home.boards
});

const mapDispatchActionsToProps = (dispatch: any) => ({
  login: () => dispatch(loginAction()),
  setBoardId: (id: string, name: string) => dispatch(setIdBoard(id, name))
});

export default connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchActionsToProps)(Home);
