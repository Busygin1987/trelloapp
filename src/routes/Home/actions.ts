import {
    FETCH_BOARDS,
    FETCH_BOARDS_SUCCESS,
    FETCH_BOARDS_FAILURE,
    DELETE_BOARDS,
    SET_ID_BOARD
} from './types';
import { Action } from 'redux';
import { RootState } from '../../store/rootReducer';
import { ThunkAction } from 'redux-thunk';


export const fetchBoard = (): ThunkAction<void, RootState, null, Action<string>> => async dispatch => {
    try {
        dispatch({
            type: FETCH_BOARDS
        });
        const res = await fetch(`https://api.trello.com/1/members/me/boards?key=${process.env.TRELLO_KEY}&token=${process.env.TRELLO_TOKEN}`)
        const data = await res.json();
    
        if (data.length > 0) {
            dispatch({
                type: FETCH_BOARDS_SUCCESS,
                payload: data
            });
        } 
    } catch (error) {
        dispatch({
            type: FETCH_BOARDS_FAILURE,
            payload: error
        });
    }
}

export const deleteBoards = () => ({ type: DELETE_BOARDS }); 
export const setIdBoard = (id: string, name: string) => ({ type: SET_ID_BOARD, payload: { id, name } }); 
    