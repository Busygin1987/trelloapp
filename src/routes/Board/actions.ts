import {
    FETCH_CARDS,
    FETCH_CARDS_SUCCESS,
    FETCH_CARDS_FAILURE,
    DELETE_CARDS,
    SET_CARD_DESC,
    DELETE_CARD_INFO
} from './types';
import { Action } from 'redux';
import { RootState } from '../../store/rootReducer';
import { ThunkAction } from 'redux-thunk';


export const fetchCardsData = (id: string): ThunkAction<void, RootState, null, Action<string>> => async dispatch => {
    try {
        dispatch({
            type: FETCH_CARDS
        });
        const res = await fetch(`https://api.trello.com/1/boards/${id}/cards?key=${process.env.TRELLO_KEY}&token=${process.env.TRELLO_TOKEN}`)
        const data = await res.json();

        if (data.length > 0) {
            dispatch({
                type: FETCH_CARDS_SUCCESS,
                payload: data
            });
        } 
    } catch (error) {
        dispatch({
            type: FETCH_CARDS_FAILURE,
            payload: error
        });
    }
}

export const deleteCards = () => ({ type: DELETE_CARDS }); 
export const deleteCardInfo = () => ({ type: DELETE_CARD_INFO }); 
export const setCardDesc = (id: string, index: number) => ({ type: SET_CARD_DESC, payload: { id, index } }); 
    