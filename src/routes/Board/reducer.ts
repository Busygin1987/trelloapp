import {
  FETCH_CARDS,
  FETCH_CARDS_SUCCESS,
  FETCH_CARDS_FAILURE,
  DELETE_CARDS,
  FetchCardsActionTypes,
  SET_CARD_DESC,
  IObject,
  DELETE_CARD_INFO
} from './types';

interface IState {
    cards: Array<object>
    error: boolean
    loading: boolean
    cardInfo: IObject | undefined
}

let initialState: IState = {
    cards: [],
    error: false,
    loading: false,
    cardInfo: {}
}

export function boardReducer(
  state = initialState,
  action: FetchCardsActionTypes
): IState {
  switch (action.type) {
    case FETCH_CARDS: {
        return {
          ...state,
          loading: true
        }
    }
    case FETCH_CARDS_SUCCESS: {
        return {
          ...state,
          loading: false,
          cards: action.payload
        }
    }
    case FETCH_CARDS_FAILURE: {
        return {
          ...state,
          loading: false,
          error: true
        }
    }
    case DELETE_CARDS: {
        return {
          ...state,
          cards: []
        }
    }
    case SET_CARD_DESC: {
      const id = action.payload.id!;
      let card;
      if (id) {
          card = state.cards.find((item: IObject) => item.id === id)
      };
      return {
        ...state,
        cardInfo: {
          ...card,
          index: action.payload.index,
          show: true
        }
      }
    }
    case DELETE_CARD_INFO: {
      return {
        ...state,
        cardInfo: {}
      }
  }
    default:
      return state
  }
}
