export const FETCH_CARDS = 'FETCH_CARDS';
export const DELETE_CARDS = 'DELETE_CARDS';
export const FETCH_CARDS_SUCCESS = 'FETCH_CARDS_SUCCESS';
export const FETCH_CARDS_FAILURE = 'FETCH_CARDS_FAILURE';
export const SET_CARD_DESC = 'SET_CARD_DESC';
export const DELETE_CARD_INFO = 'DELETE_CARD_INFO';

interface FetchCards {
  type: typeof FETCH_CARDS
}

interface FetchCardsSuccess {
  type: typeof FETCH_CARDS_SUCCESS
  payload: Array<Object>
}

interface FetchCardsFailure {
  type: typeof FETCH_CARDS_FAILURE
  payload?: []
}

interface DeleteCards {
  type: typeof DELETE_CARDS
  payload?: any
}

interface SetCardDesc {
  type: typeof SET_CARD_DESC
  payload: IObject
}

interface DeleteCardInfo {
  type: typeof DELETE_CARD_INFO
  payload?: any
}

export interface IObject {
  id?: string
  desc?: string
  dateLastActivity?: string
  index?: number
  show?: boolean
}

export type FetchCardsActionTypes = DeleteCardInfo | SetCardDesc | FetchCardsSuccess | FetchCards | FetchCardsFailure | DeleteCards