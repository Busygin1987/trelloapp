import React from 'react';
import Typography from '../../components/Typography';
import { connect } from 'react-redux';
import { fetchCardsData, setCardDesc, deleteCardInfo } from './actions';
import Button from '../../components/Button';
import PopUp from '../../modules/PopUp';
import { IObject } from './types';
import { WrapContainer, WrapListCards, Control, ControlButtonFirst, ButtonControlFirst, ControlButtonSecond, ButtonControlSecond, ListCards, CardContainer, Line } from './styled';
import { withRouter, RouteComponentProps } from 'react-router-dom';

interface StateProps {
  boardId: string
  boardName: string
  cards: Array<object>
  cardInfo: IObject
}
     
interface DispatchProps {
  fetchCards: (id: string) => void
  setCardDescAction: (id: string, idx: number) => void
  deleteCardInfoAction: () => void
}

interface OwnProps extends RouteComponentProps<any> {}

type Props = StateProps & DispatchProps & OwnProps

const Board: React.FC<Props> = ({
  fetchCards,
  boardId,
  boardName,
  cards,
  cardInfo,
  setCardDescAction,
  deleteCardInfoAction,
  history
}) => {
 
  React.useEffect(() => {
    fetchCards(boardId);
  }, []);

  return (
    <WrapContainer>
      <WrapListCards>
        <Control>
          <ControlButtonFirst>
            <ButtonControlFirst onClick={() => history.push('/')}><Typography fs={20}>Boards</Typography></ButtonControlFirst>
          </ControlButtonFirst>
          <ControlButtonSecond>
            <ButtonControlSecond><Typography fs={20}>{boardName}</Typography></ButtonControlSecond>
          </ControlButtonSecond>
        </Control>
        <Typography fs={32} fw={600}>List cards</Typography>
        <ListCards>
          {
            cards.map((card: any, index, arr) => {
              return <React.Fragment key={card.id}>
                <CardContainer>
                  <Typography>{`${card.name} #${index + 1}`}</Typography>
                  <Button onClick={() => setCardDescAction(card.id, index + 1)}>Show details</Button>
                </CardContainer>
                {index !== arr.length - 1 ? <Line /> : null}
              </React.Fragment>
            })
          }
        </ListCards>
      </WrapListCards>
      {cardInfo.show && <PopUp hidePopUp={deleteCardInfoAction} cardInfo={cardInfo} />}
    </WrapContainer>
  );
}

const mapStateToProps = (state: any) => ({
  boardId: state.home.currentBoard.id,
  boardName: state.home.currentBoard.name,
  cards: state.board.cards,
  cardInfo: state.board.cardInfo
});

const mapDispatchActionsToProps = (dispatch: any) => ({
  fetchCards: (id: string) => dispatch(fetchCardsData(id)),
  setCardDescAction: (id: string, idx: number) => dispatch(setCardDesc(id, idx)),
  deleteCardInfoAction: () => dispatch(deleteCardInfo())
});

export default withRouter(connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchActionsToProps)(Board));
