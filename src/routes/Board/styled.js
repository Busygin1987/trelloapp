import styled from 'styled-components';

export const WrapContainer = styled.div`
  height: auto;
`;

export const ListCards = styled.div`
  height: 100%;
  border: 1px solid #000;
  margin-top: 20px;
`;

export const CardContainer = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 15px;
`;

export const Line = styled.hr`
  border-bottom: 1px solid #000;
`;

export const WrapListCards = styled.div`
  width: 90%;
  height: 100%;
  margin-left: auto;
  margin-right: auto;
  margin-top: 20px;
`;

export const ControlButtonFirst = styled.div`
  position: relative;
  width: 154px;
  height: 40px;
  background-color: #000;
  -webkit-clip-path: polygon(0% 0%, 84% 0, 100% 50%, 85% 100%, 0% 100%);
clip-path: polygon(0% 0%, 84% 0, 100% 50%, 85% 100%, 0% 100%);
`;

export const ButtonControlFirst = styled.button`
  padding-right: 20px;
  position: absolute;
  top: 2px; 
  left: 2px;
  width: 150px;
  height: 36px;
  border: none;
  outline: none;
  cursor: pointer;
  -webkit-clip-path: polygon(0% 0%, 84% 0, 100% 50%, 85% 100%, 0% 100%);
  clip-path: polygon(0% 0%, 84% 0, 100% 50%, 85% 100%, 0% 100%);

  &:active {
        background-color: #A9A9A9;
  }
`;

export const ControlButtonSecond = styled.div`
  position: absolute;
  left: 129px;
  top: 0;
  width: 204px;
  height: 40px;
  background-color: #000;
  -webkit-clip-path: polygon(89% 1%, 100% 50%, 90% 100%, 0% 100%, 12% 50%, 0% 0%);
clip-path: polygon(89% 1%, 100% 50%, 90% 100%, 0% 100%, 12% 50%, 0% 0%);
`;

export const ButtonControlSecond = styled.button`
  /* padding-right: 20px; */
  overflow: hidden;
  text-overflow: ellipsis;
  position: absolute;
  top: 2px; 
  left: 2px;
  width: 200px;
  height: 36px;
  border: none;
  outline: none;
  cursor: normal;
  -webkit-clip-path: polygon(89% 1%, 100% 50%, 90% 100%, 0% 100%, 12% 50%, 0% 0%);
clip-path: polygon(89% 1%, 100% 50%, 90% 100%, 0% 100%, 12% 50%, 0% 0%);
`;

export const Control = styled.div`
  margin: 30px 0;
  position: relative;
`;