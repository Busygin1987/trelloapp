import React from 'react';
import Typography from '../../components/Typography';
import ButtonClose from '../../components/ButtonClose';
import { Header, WrapContainer, ContainerSelect, StyledSelect, StyledOption, StyledButton, Body } from './styled';
import { RouteComponentProps, withRouter } from 'react-router-dom';

interface BoardType {
     name: string
     id: string
}

interface OwnProps extends RouteComponentProps<any> {
     boards: Array<BoardType>
     setBoardId: (id: string, name: string) => void
}

const SelectBoard: React.FC<OwnProps> = ({ boards, setBoardId, history }) => {
     
     const getBoardInfo = () => {
          const select = document.querySelector('#boardSelect') as HTMLSelectElement;
          const opt = select.options[select.selectedIndex];
          const id = opt.id;
          const name = opt.text;
          setBoardId(id, name);
          history.push(`./board/${id}`)
     }

     return (
          <WrapContainer>
          <Header>
               <Typography>Select the board</Typography>
               <ButtonClose onClick={() => {}} />
          </Header>
          <Body>
               <ContainerSelect>
                    <StyledSelect id="boardSelect">
                         {boards.map((board: BoardType, index: number) => {
                              return <StyledOption id={board.id} key={board.id}>{`${board.name} #${index + 1}`}</StyledOption>
                         })}
                    </StyledSelect>
               </ContainerSelect>
               <StyledButton onClick={getBoardInfo}>Confirm</StyledButton>
          </Body>        
     </WrapContainer>
     )
};
 
export default withRouter(SelectBoard);