import styled from 'styled-components';

export const WrapContainer = styled.div`
     width: 75%;
     height: 75%;
     border: 2px solid #000;
`;

export const Header = styled.div`
     display: flex;
     justify-content: space-between;
     height: 30px;
     padding: 1rem;
`;
export const Body = styled.div`
     display: flex;
     justify-content: center;
     align-items: center;
     position: relative;
     height: 347px;
`;

export const StyledSelect = styled.select`
     width: 340px;
     height: 100%;
     border-radius: 6px;
     border: 2px solid #000;
     font-weight: 600;
     padding: 5px;
     outline: none;
     cursor: pointer;
`;

export const ContainerSelect = styled.div`
     height: 34px;
     padding: 5px;
     margin-bottom: 60px;
`;

export const StyledOption = styled.option`
     font-weight: 600;
`;

export const StyledButton = styled.button`
     position: absolute;
     right: 20px;
     bottom: 20px;
     width: 120px;
     height: 30px;
     background-color: #D3D3D3;
     color: #000;
     font-size: 20px;
     font-weight: 500;
     border-radius: 8px;
     border: 2px solid #000;
     outline: none;
     cursor: pointer;
`;
