import styled from 'styled-components';
import Button from '../../components/Button';

export const PopUpContainer = styled.div`
    width: auto;
    height: auto;
    background-color: rgba(0, 0, 0, 0.3);
    position: fixed;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    display: flex;
    justify-content: center;
    align-items: center;
`;

export const PopUp = styled.div`
    width: 550px;
    min-height: 400px;
    background-color: #fff;
    border: 1px solid #000;
    padding: 1.5rem;
    position: relative;
`;

export const Description = styled.div`
    margin: 40px 0;
`;

export const StyledButton = styled(Button)`
    bottom: 20px;
    right: 20px;
    position: absolute;
`;