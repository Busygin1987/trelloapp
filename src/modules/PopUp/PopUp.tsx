import React from 'react';
import moment from 'moment';
import Typography from '../../components/Typography';
import { IObject } from '../../routes/Board/types';
import { PopUpContainer, Description, PopUp, StyledButton } from './styled';

interface OwnProps {
     hidePopUp: (status: boolean) => void
     cardInfo: IObject
}

const PopUpComponent: React.FC<OwnProps> = ({
    hidePopUp,
    cardInfo: {
        desc,
        dateLastActivity = '',
        index
    }
}) => {
    return (
        <PopUpContainer>
            <PopUp>
                <Typography fw={600} fs={28}>Card title #{index} Details</Typography>
                <Description>
                    <Typography fw={600} fs={20}>Description: </Typography>
                    <Typography fs={20}>{desc}</Typography>
                </Description>
                <div>
                    <Typography fs={20} fw={600}>Last activity: </Typography>
                    <Typography fs={20}>{moment(dateLastActivity).fromNow()}</Typography>
                </div>
                <StyledButton onClick={() => hidePopUp(false)}>Confirm</StyledButton>
            </PopUp>
        </PopUpContainer>
    )
};
 
export default PopUpComponent;