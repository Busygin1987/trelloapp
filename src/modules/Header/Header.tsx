import React from 'react';
import { connect } from 'react-redux';

import Typography from '../../components/Typography';
import Button from '../../components/Button';
import { loginAction, logoutAction } from './actions';
import { WrapHeader } from './styled';
import { RouteComponentProps, withRouter } from 'react-router-dom';

interface StateProps {
     loginStatus: boolean
}

interface DispatchProps {
     login: () => void
     logout: () => void
}

interface OwnProps extends RouteComponentProps<any> {}

type Props = StateProps & DispatchProps & OwnProps

const Header: React.FC<Props> = ({ login, loginStatus, logout, history }) => (
     <WrapHeader>
          <Typography
               fw={'bold'}
               fs={32}
          >
               Trello Observer
          </Typography>
          <Button onClick={() => { 
               if (loginStatus) {
                    logout();
                    history.push('/');
               } else {
                    login();
               }}
          }>
               <Typography>
                    {loginStatus ? 'Logout' : 'Login via Trello'}
               </Typography>
          </Button>
     </WrapHeader>
);


const mapStateToProps = (state: any) => ({
     loginStatus: state.login.loginStatus
 });
 
const mapDispatchActionsToProps = (dispatch: any) => ({
     login: () => dispatch(loginAction()),
     logout: () => dispatch(logoutAction()),
});
 
export default withRouter(connect(mapStateToProps, mapDispatchActionsToProps)(Header));