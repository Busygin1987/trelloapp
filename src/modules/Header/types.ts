export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';

interface ILogin {
  type: typeof LOGIN
}

interface ILogout {
  type: typeof LOGOUT
}

export type LoginActionTypes =  ILogin | ILogout