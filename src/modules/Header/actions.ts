import { LOGIN, LOGOUT } from './types';
import { Action } from 'redux';
import { RootState } from '../../store/rootReducer';
import { ThunkAction } from 'redux-thunk';
import { fetchBoard, deleteBoards } from '../../routes/Home/actions';
import { deleteCards } from '../../routes/Board/actions';


export const loginAction = (): ThunkAction<void, RootState, null, Action<string>> => dispatch => {
    dispatch({ type: LOGIN });
    dispatch(fetchBoard());
}

export const logoutAction = (): ThunkAction<void, RootState, null, Action<string>> => dispatch => {
    dispatch({ type: LOGOUT });
    dispatch(deleteBoards());
    dispatch(deleteCards());
}   