import styled from 'styled-components';

export const WrapHeader = styled.div`
    width: 100%;
    height: 80px;
    border: 2px solid #000;
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 1rem;
    box-sizing: border-box;
`;