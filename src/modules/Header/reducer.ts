import { LOGIN, LOGOUT, LoginActionTypes } from './types';

interface IState {
    loginStatus: boolean
}

let initialState: IState = {
    loginStatus: false
}

export function loginReducer(
  state = initialState,
  action: LoginActionTypes
): IState {
    switch (action.type) {
      case LOGIN: {
        return {
          loginStatus: true
        }
      }
      case LOGOUT: {
        return {
          loginStatus: false
        }
      }
    
      default:
        return state
    }
    
}
