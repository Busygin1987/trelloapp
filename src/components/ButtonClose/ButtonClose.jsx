import styled from 'styled-components';
import close from '../../assets/close.png';

const ButtonClose = styled.span`
    width: 30px;
    height: 30px;
    background-image: url(${close});
    color: #000;
    cursor: pointer;
`;

export default ButtonClose;