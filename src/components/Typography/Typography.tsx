import styled from "styled-components";

interface TypoProps { 
    fw?: string
    fs?: number | string
    color?: string
}

export default styled.span`
    font-weight: ${(p: TypoProps) => p.fw || 'normal'};
    font-size: ${(p: TypoProps) => p.fs ? p.fs + 'px' : '24px'};
    display: inline-block;
    color: ${(p: TypoProps) => p.color ? p.color : '#000'};
`;