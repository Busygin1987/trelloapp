import styled from 'styled-components';

const Button = styled.button`
    padding: 6px 20px;
    border: 2px solid #000;
    outline: none;
    color: #000;
    border-radius: 10px;
    background-color: #D3D3D3;
    cursor: pointer;
    &:active {
        background-color: #A9A9A9;
    }
`;

export default Button;