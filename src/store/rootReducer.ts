import { combineReducers } from 'redux';
import { homeReducer } from '../routes/Home/reducer';
import { loginReducer } from '../modules/Header/reducer';
import { boardReducer } from '../routes/Board/reducer';

const rootReducer = combineReducers({
    home: homeReducer,
    login: loginReducer,
    board: boardReducer
})

export type RootState = ReturnType<typeof rootReducer>
export default rootReducer