import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter } from 'react-router-dom';
import thunk from 'redux-thunk';

import App from './App';
import rootReducer from './store/rootReducer';
import GlobalStyles from './globalStyles.js';


const store = createStore(rootReducer, applyMiddleware(thunk));

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <App />
            <GlobalStyles />
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);