import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Header from './modules/Header';
import Home from './routes/Home';
import Board from './routes/Board';

const App: React.FC = () => {
  return (
    <Switch>
      <Route path='/'>
          <Header />
          <Switch>
              <Route exact path="/board/:id">
                  <Board />
              </Route>  
              <Route path="/">
                  <Home />
              </Route>           
          </Switch>
      </Route>
    </Switch>
  );
}

export default App;
